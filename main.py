# Premier cours de Python avec Mr.ARTZ

"""
# print("test")
Ceci est
un commentaire
multiligne
"""

""" 
Cours Python 18/10/2022
Vincent ARTZ artzvincent@gmail.com

 méthodologie et programme 
séquence + code + exercice


Notions
Problèmes à résoudre avec challenges
projet
Restitution des fichiers ----> github etc…

Créé par un informaticien et un mathématicien, cadeau au monde créé en 1989, apparu au public en 1991.
Qualités : 
portabilité (fonctionne partout ou presque), logique (cohérence), deviner les méthodes (avant même de les utiliser) ;
productivité des développeurs (anecdote : un programme python est généralement 1/5 plus petit qu’un code java ou c++/ travail plus rapide, répondre plus facilement aux besoins du marché) ;
Maintenance, débogage (avec avantages et inconvénients) ;
Bibliothèques étendues (standard + communauté) ;
Qualité du logiciel : accès sur la lisibilité, cohérence et la qualité (uniforme et lisibilité forte) ;
Python a une nature multiparadigme, langage de scripts, exploitation de la POO, polyvalent ;
Intégration logicielle ;
Il peut être intégré et étendu à de nombreux autres langages, python aide à l’interconnexion logiciel ;
Satisfaction et plaisir pour la techno ;

Inconvénients :
Minecraft,
Vitesse d’exécution : peut-être plus lent que les langages compilés 
« Portabilité » qui se paye par des temps d’exécutions plus longs (PyPy x4 ?)
Contexte :
Google, Dropbox, NASA, Netflix, Spotify, Battlefield 2 etc… 
Programation de systemes, web, APIs, GUI, Jeux, Robotique, BDD

Environnements :
Logiciels : Pycharm -> Jetsorm Community 
Python 2 et 3 
EOL pour Python 2

# if 5>2: # Exemple de condition
    # print("pourquoi") # Print

# Les variables ne sont pas typées
# x = 5 # Possède le type int automatiquement
# y = "Salut" # Possède le type string automatiquement

# print(x)
# print(y)

a = str(5)
b = int(5)
c = float(5)

# print(a)
# print(b)
# print(c)

# Afficher le type d'une variable
# print(type(a))

d = 'Manelle'
e = "Manelle"

# print(d)
# print(e)



Création nom de variable : commence par une lettre, ne peut pas démarrer par un nombre, possède uniquement
des caractères alpha et l'underscore, variable sensible à la casse. 
Convention : camelCase, PascalCase, snake_Case
mavar
ma_var
MAVAR
myVar

pas bon :
_ma_var
2mavar

Assigner plusieurs valeurs
x,y,z = "Fruits", "Légumes", "Lait"
print(x)
print(y)
print(z)
output : "Fruits", "Légumes", "Lait"

x,y,z = "Fruits"
print(x)
print(y)
print(z) 
output : Fruits

Liste, tuple

Fruits = ["Pomme", "Banane", "Ananas"]
x,y,z = Fruits
print(x)

concaténation 
x = "ceci est"
y = " un"
z =  " test"
print(x + y + z)

addition deux variables
x = 10
y = 30
print(x+y)
output : 40 

addition deux variables de types differents
x = 10
y = "test"
print(x+y)
output : TypeError: unsupported operand type(s) for +: 'int' and 'str'

variable globale
a = "genial"

def myFunction():
                print("python est"+a)
                
myFunction()


a = "genial"

def myFunction():
    global a
    a = "superbe"
    print("python est " + a)

myFunction()
print(a) output : superbe (parce que global)


TYPES DE DONNÉES 
str = chaine
numeric = int, float, complex
sequences = list, tuple, range
Mapping type = dict
Set Types = set, frozenset
booléens = bool
binary = bytes, bytearray, memoryview
none type = NoneType

pour afficher le type : print(type(var))

tuple 
x=("fruits", "légumes")

range
x = range(6)

dict
x={"nom": "Laamarti", "age":40}

set 
x={"fruits", "légumes"}

boolean 
x = True

bytes 
x=b"Salut"
x=bytearray(5)
x=memoryview(bytes(5))
x = None

NOMBRES
int (x=1), float (z=4.5), complex (z=1j)

EXPOSANTS 
x = 35e3
y = 12e4
Z + 87.5e100

print(type(z))
x = 5j
print(type(x))

x=1
y=3.5
z=1j

a=float(x)
b=int(y)
c=complex(x)



print(a)
print(b)
print(c)

print(type(a))
print(type(b))
print(type(c))
import random

print(random.randint(1, 10))
GUI, Tkinter,

interfaces graphiques python : PyTQ5/PySide, WxPython, Kivy etc...

bibliothèques = collections de fonctions et d'objets qui vont fournir des fonctionnalités qui vont enrichir les
capacités d'un langage

maths : par exemple factorial 

from math import factorial 
factorial(5)
output: 120

variable string avec chaines en multilignes fonctionne 
a='''test
de
cette
variable multiligne'''
print(a)

a = "hello ça va"
print(a[6])

for x in "Manelle":
    print(x)
    
a = "Hello, ça va"
print(len(a))

TROUVER OCCURENCE DANS LA CHAINE DE CARACTERES
a = "Hello, ça va? mais ça va bien ou pas"
print("va" in a)

a = "Hello, ça va? mais ça va bien ou pas"
if "slkd,ql" not in a:
    print("pas trouvé")
    
SLICE
x = "salut les amis"
print(x[5:])

x = "salut les amis"
print(x[-5:-2])


prenom = "Pierre"
age = 20
majeur = True
compteEnBanque = 201135.384
amis = ["Marie", "Julien", "Adrien"]
parents = ("Marc", "Caroline")

print(prenom)
print(age)
print(majeur)
print(compteEnBanque)
print(amis)
print(parents)

nombre = 15
print("Le nombre est " + str(nombre))

a = 2
b = 6
c = 3

print(a,b,c, sep="+")

liste = range(3)
list2 = range(5)

print(list(list2))

prenom = "string"

def checkIfString(var):
    if isinstance(prenom, str):
        print("la variable est une string")
    else:
        print("la variable est un entier")


checkIfString(prenom)

TOLOWER ET TO UPPER
a = "Hello"
print(a.upper())
print(a.lower())


SUPPRESSION DES ESPACES AVANT ET APRES
a = "Bonjour ça va ? "
print(a.strip(" "))

SUPPRIMER DES CARACTERES DANS UNE STRING
a = "Bonjour ça va ? "
print(a.replace("j", "o"))

SEPARER DES ELEMENTS PAR UN CARACTERE
a = "Bonjour ça va ? "
print(a.split(","))

CONCATENATION
a = "teste "
b = "moi"
c = a + b
print(c)

CONCATENATOPN DE DEUX VARIABLE DE TYPES DIFFERENTS STRING INT
age = 22
text = "J'ai " + str(age) + " ans"
print(text)
txt = "j'ai "
print(txt + format(age))

BINDING D'AGE SUR UNE STRING GRACE AUX {} BRACKETS
age = 22
age2 = 27
age3 = 35
txt = "Voici mon age {1} dans 5 ans j'aurai {0} ans et beaucoup plus tard {2} ans"
print(txt.format(age, age2, age3))

QUOTES DANS UNE STRING
a = "Bonjour voici \"le\" merveilleux langage"
print(a)

b = "voici un exemple de phrase exemple"
c = b.find("exemple")
print(c)

OUTPUT BOOLEANS
print(10>3)
print(10==9)

x = 0
y = ""
output false

u = 6
i = "string"
output true

UTILISATION CLASSE
class maClass():
    def __len__(self):
        return 0
monObjet = maClass()
print(bool(monObjet))

OPERATEURS 

a = 2
b = a ** 2400
e = 14
c = 2 % 14
print(b)
print(c)

a = 2
e = 14
c = 2 % 14
a+=3
print(a)

x = 5
print(not(x>3 and x<10))

x = ["fruits", "légumes"]
y = ["fruits", "légumes"]
z = x

print(x is y)
print(x is z)
print(x == y)

REPLACE

phrase = "Bonjour"
print(phrase.replace("Bonjour","Bonsoir"))

phrase = "Bonjour et bonjour"
nouvellePhrase = phrase.replace("Bonjour", "Bonsoir")
print(nouvellePhrase)

names = "Pierre, Julien, Lucien, Marie, Zack"
namesSplit = names.split(", ")
namesSplit.sort()
namesOrdered = ", ".join(namesSplit)
print(namesOrdered)


solution propre d'un collègue : 

def sortByAlphabeticalOrder(prenoms):
  textArray = prenoms.split(", ")
  return ", ".join(sorted(textArray))

print(sortByAlphabeticalOrder("Pierre, Julien, Anne, Marie, Lulien"))

COUNT NOMNBRES D'ELEMENTS DANS LA LISTE
listNames = ["Pierre", "Julien", "Lucien", "Marie" , "Zack"]
print(len(listNames))

listNames = list(("Pierre", "Julien", "Lucien", "Marie", "Zack"))
print(len(listNames))
on accède avec les index et on peut utiliser des index negatifs

listNames = ["Pierre", "Julien", "Lucien", "Marie", "Zack"]
print(listNames[0:4])

listNames[1] = "Manelle"
print(listNames)

listNames = ["Pierre", "Julien", "Lucien", "Marie", "Zack"]
listNames[1:3] = ["Manelle", "Ghislain"]
print(listNames)

listNames = ["Pierre", "Julien", "Lucien", "Marie", "Zack"]

del listNames[0]
print(listNames)
listNames.pop(1)

# LOOPS

# FOR
listNames = ["Pierre", "Julien", "Lucien", "Marie", "Zack"]

for i in range(len(listNames)):
    print(listNames[i])$

[print(x) for x in listNames]
    
# WHILE
x = 0
while x< len(listNames):
    print(listNames[x])
    x = x+1

listNames = ["Pierre", "Julien", "Lucien", "Marie", "Zack"]
fruits = ["Banane", "Pomme", "Kiwi", "Ananas"]
for x in fruits:
    if "a" in x:
        fruits.append(listNames)

fruits = ["Banane", "Pomme", "Kiwi", "Ananas"]

newList = [x for x in fruits if x != "Banane"]
print(newList)



fruits = ["Banane", "Pomme", "Kiwi", "Ananas"]

newList = [x for x in range(20) if x<10]
print(newList)

from math import pi
rayon = 10
volume = (4*pi/3)*rayon**3

print("le volume de la sphère est "+ str(volume))µ

EXERCICE 11

def checkIfGreaterThanA(number: int):
    if(isinstance(number, int)):
        if(number > 10):
            print(str(number) + " is greater than 10")
        elif(number < 10):
            print(str(number) + " is smaller than 10")
    else:
        print(str(number) + " is not a number")

print("drop a number : ")
yourNumber = input()
checkIfGreaterThanA(int(yourNumber))


def customSort(n):
    return abs(n-50)

numbers = [58,10,88,42,78,81]
numbers.sort(key = customSort)
print(numbers)
"""
